<?php
    require_once("control/ControleLogin.php");

    $order_by = $_GET['order_by'] ?? null;

    $controle = new ControleLogin();
    if (isset($_GET['nome']) && $_GET['nome'] != '') {
       $nome = "'". $_GET['nome'] ."%'";
       $logins = $controle->consultarNome($nome, $order_by) ?: "Usuários não encontrados!";
    } else {
        $logins = $controle->selecionarTodos($order_by) ?: "Nenhum usuário cadastrado!"; 
    }

    if (is_array($logins)) {
        echo "<table class='table'>
                    <tr>
                        <th>Nome</th>
                        <th>Email</th>
                        <th>Senha</th>
                        <th>Idade</th>
                        <th>Sexo</th>
                    </tr>
                ";
            foreach ($logins as $login) {
                echo "<tr>";
                    echo "<td>{$login->getNome()}</td>";
                    echo "<td>{$login->getEmail()}</td>";
                    echo "<td>{$login->getSenha()}</td>";
                    echo "<td>{$login->getIdade()}</td>";
                    echo "<td>{$login->getSexo()}</td>";
                echo "</tr>";
            }
        echo "</table>"; 
    } else {
        echo $logins;
    }
    
