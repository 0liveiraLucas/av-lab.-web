<?php
    require_once("Conexao.php");
    require_once("model/Login.php");
    
    class ControleLogin
    {
        public function consultarNome($nome, $order_by)
        {
            try{
                $con = new Conexao("control/banco.ini");
                if (strlen($order_by) == 1) 
                    $order_by .= 'x';
                $comando = $con->getPDO()->prepare($this->getOrderBySQL($order_by, "SELECT nome, senha, email, sexo, TIMESTAMPDIFF(YEAR, dataNasc, NOW()) AS idade FROM usuario WHERE nome LIKE $nome")); 
                if($comando->execute()){
                    $retorno = $comando->fetchAll(PDO::FETCH_CLASS, "Login");
                }else{
                    $retorno = null;
                }
            } catch (PDOException $PDOex) {
                echo("Erro no banco de dados: {$PDOex->getMessage()}");
            } catch (Exception $e) {
                echo("Erro encontrado: {$e->getMessage()}");
            } finally {
                $con->encerrarConexao();
                return $retorno;
            }
        }

        public function selecionarTodos($order_by = null)
        {
            try {
                $con = new Conexao("control/banco.ini");
                $comando = $con->getPDO()->prepare($this->getOrderBySQL($order_by, "SELECT nome, senha, email, sexo, TIMESTAMPDIFF(YEAR, dataNasc, NOW()) AS idade FROM usuario"));
                if ($comando->execute()) {
                    $retorno = $comando->fetchAll(PDO::FETCH_CLASS, "Login");
                } else {
                    $retorno = null;
                }
            } catch (PDOException $PDOex) {
                echo("Erro no banco de dados: {$PDOex->getMessage()}");
            } catch (Exception $ex) {
                echo("Erro encontrado: {$ex->getMessage()}");
            } finally {
                $con->encerrarConexao();
                return $retorno;
            }
        }

        private function getOrderBySQL($order_by, $select)
        {
            if ($order_by == 'nome') {
                $select .= " ORDER BY nome;";
            } elseif ($order_by == 'idade') {
                $select .= " ORDER BY idade;";
            } elseif ($order_by == 'm') {
                $select .= " WHERE sexo='M';";
            } elseif ($order_by == 'f') {
                $select .= " WHERE sexo='F';";
            } elseif ($order_by == 'mx') {
                $select .= " AND sexo='M';";
            } elseif ($order_by == 'fx') {
                $select .= " AND sexo='F';";
            }

            return $select;
        }
    }
