<?php
    class Conexao
    {
        private $usr;
        private $pwd;
        private $host;
        private $driver;
        private $nome;
        private $pdo;

        public function __construct($arquivo_ini)
        {
            try{
                if(file_exists($arquivo_ini))
                {
                    $conf = parse_ini_file($arquivo_ini);
                    $this->usr = $conf['usuario'];
                    $this->pwd = $conf['senha'];
                    $this->host = $conf['host'];
                    $this->driver = $conf['driver'];
                    $this->nome = $conf['nome'];
                    
                    if($this->driver == 'mysql')
                    {
                        $this->pdo = new PDO("{$this->driver}:host={$this->host};dbname={$this->nome}",
                                               $this->usr, $this->pwd);

                        $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                    }else{
                        die("SGBD incompatível!");
                    }
                }else{
                    die("Arquivo de configuração inexistente");
                }
            } catch(PDOException $e) {
                echo("
                    <script>
                        alert('Erro na conexão com o banco: {$e->getMessage()}');
                    </script>
                ");
            }
        }

        public function encerrarConexao()
        {
            $this->pdo = null;
        }

        public function getPDO()
        {
            return $this->pdo;
        }
    }
