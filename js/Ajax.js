class Ajax {
    constructor() {
        this.ajax = null;
        try {
            if (window.XMLHttpRequest) {
                this.ajax = new window.XMLHttpRequest();
            } else if (window.ActiveXObject("Msxml2.XMLHTTP")) {
                this.ajax = new window.ActiveXObject("Msxml2.XMLHTTP");
            } else if (window.ActiveXObject("Microsoft.XMLHTTP")) {
                this.ajax = new window.ActiveXObject("Microsoft.XMLHTTP");
            } else {
                throw "Browser não compatível";
            }
        } catch (e) {
            console.log(e.getMessage());
        }
    }

    getAjax() {
        return this.ajax;
    }

    requisitarDados(arquivo, elemento) {
        if (this.ajax) {
            this.ajax.onreadystatechange = function () {
                if (this.readyState == 4) {
                    if (this.status == 200 || this.status == 304) {
                        elemento.innerHTML = this.responseText;
                    } else {
                        console.log("Erro no servidor");
                    }
                }
            };
            this.ajax.open("GET", arquivo);
            this.ajax.send(null);
        }
    }

}