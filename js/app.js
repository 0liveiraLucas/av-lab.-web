let a = new Ajax();
let mostrar = document.getElementById("mostrar");
let entrada = document.getElementById("nome");
let botoesDIV = document.getElementById("botoes");
let order_by = null;

a.requisitarDados("http://localhost/av-lab.-web/consultar.php", mostrar);

entrada.addEventListener('input', function (evento) {
    a.requisitarDados("http://localhost/av-lab.-web/consultar.php?nome=" + entrada.value + "&order_by=" + order_by, mostrar);
});

botoes.onclick = function (evento) {
    if (evento.target && evento.target.nodeName == "BUTTON") {
        order_by = evento.target.value;
        a.requisitarDados("http://localhost/av-lab.-web/consultar.php?nome=" + entrada.value + "&order_by=" + order_by, mostrar);
    }
}
